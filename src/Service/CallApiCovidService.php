<?php

namespace App\Service;

use Symfony\Contracts\HttpClient\Exception\TransportExceptionInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CallApiCovidService
{
    protected $client;

    public function __construct(HttpClientInterface $client){
        $this->client = $client;
    }

    public function getFranceCovidData() : array{
        $reponse = $this->getApi( "data/live/france");

        return $reponse->toArray();
    }

    public function getDataCovidByDep($dep){
        $reponse = $this->getApi("data/live/departement/".$dep);
        return $reponse->toArray();

    }

    public function getDataCovidAllDep(){
        $reponse = $this->getApi("data/live/departements");
        return $reponse->toArray();

    }

    public function getDataCovidByDate(){
        $reponse = $this->getApi("data/france-by-date/11-10-2021");
        return $reponse->toArray();

    }

    private function getApi(string $var){
        try {
            $reponse = $this->client->request(
                "GET",
                "https://coronavirusapifr.herokuapp.com/".$var
            );
        } catch (TransportExceptionInterface $e) {
        }
        return $reponse;
    }

}