<?php

namespace App\Controller;

use App\Service\CallApiCovidService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{

    /**
     * @Route("/home", name="home")
     */
    public function index(CallApiCovidService $apiCovidService): Response
    {

        return $this->render('home/index.html.twig', [
            'data' => $apiCovidService->getFranceCovidData()
        ]);
    }

    /**
     * @Route("/home/departements", name="departements")
     */
    public function departements(CallApiCovidService $apiCovidService): Response
    {

        return $this->render('home/index.html.twig', [
            'departements' => $apiCovidService->getDataCovidAllDep(),
            'data' => $apiCovidService->getFranceCovidData()
        ]);
    }

    /**
     * @Route("/home/departement/{departement}", name="app_department")
     */
    public function departement(CallApiCovidService $apiCovidService, $departement): Response
    {

        return $this->render('home/index.html.twig', [
            'departement' => $apiCovidService->getDataCovidByDep($departement)
        ]);
    }


}
